export class WikipediaResultItem {
    private term: string;
    private url: string;
    private blurb: string;

    constructor(term: string, link: string, summary: string) {
        this.blurb = summary;
        this.term = term;
        this.url = link;
    }

    public getTerm(): string {
        return this.term;
    }

    public getLink(): string {
        return this.url;
    }

    public getSummary(): string {
        return this.blurb;
    }
}