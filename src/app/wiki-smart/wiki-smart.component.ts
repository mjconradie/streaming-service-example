import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { Subject } from 'rxjs/Subject';
import { WikipediaService } from '../services/wikipedia.service';
import { WikipediaResultItem } from '../models/wikipedia-result-item.model';

@Component({
  selector: 'app-wiki-smart',
  templateUrl: './wiki-smart.component.html',
  styleUrls: ['./wiki-smart.component.css'],
  providers: [ WikipediaService ]
})
export class WikiSmartComponent implements OnInit {
  items: Observable<WikipediaResultItem[]>;
  private searchTermStream = new Subject<string>();

  constructor (private wikipediaService: WikipediaService) {}

  search(term: string) { this.searchTermStream.next(term); }

  ngOnInit() {
    this.items = this.searchTermStream
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap((term: string) => this.wikipediaService.search(term));
  }
}

