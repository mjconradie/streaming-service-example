import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { WikiSmartComponent } from './wiki-smart/wiki-smart.component';
import { requestOptionsProvider } from './default-request-options.service';
import { ValueOrDefaultPipe } from './pipes/value-or-default.pipe';

@NgModule({
  declarations: [
    AppComponent,
    WikiSmartComponent,
    ValueOrDefaultPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    // InMemoryWebApiModule.forRoot(HeroData)
  ],
  providers: [ requestOptionsProvider ],
  bootstrap: [AppComponent]
})
export class AppModule { }