import { Injectable } from '@angular/core';
import { Jsonp, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';

import { WikipediaResultItem } from '../models/wikipedia-result-item.model';

export const enum WIKIPEDIA_RESPONSE_PARTS {
  SEARCH_TERM = 0,
  TERMS = 1,
  BLURBS = 2,
  LINKS = 3
}

@Injectable()
export class WikipediaService {
  constructor(private jsonp: Jsonp) {}

  search (term: string) {

    const wikiUrl = 'http://en.wikipedia.org/w/api.php';
    const params = new URLSearchParams();
    params.set('search', term); // the user's search value
    params.set('action', 'opensearch');
    params.set('format', 'json');
    params.set('callback', 'JSONP_CALLBACK');

    // TODO: Add error handling
    return this.jsonp
               .get(wikiUrl, { search: params })
               .map(response => {
                 console.dir(response);
                 const result: WikipediaResultItem[] = [];
                 const json = response.json();
                 const total = json &&
                              json[WIKIPEDIA_RESPONSE_PARTS.TERMS] &&
                              json[WIKIPEDIA_RESPONSE_PARTS.TERMS].length
                              ? json[WIKIPEDIA_RESPONSE_PARTS.TERMS].length : 0;

                 for (let idx = 0; idx < total; idx++) {
                    result.push(this.newResultItem(json, idx));
                 }
                 return result;
               });
  }

  private newResultItem(json: any, itemIndex: number): WikipediaResultItem {
    const term = json[WIKIPEDIA_RESPONSE_PARTS.TERMS][itemIndex];
    const url = json[WIKIPEDIA_RESPONSE_PARTS.LINKS][itemIndex];
    const blurb = json[WIKIPEDIA_RESPONSE_PARTS.BLURBS][itemIndex];
    return new WikipediaResultItem(term, url, blurb);
  }
}
