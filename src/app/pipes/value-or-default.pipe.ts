import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'valueOrDefault'
})
export class ValueOrDefaultPipe implements PipeTransform {

  transform(value: any, defaultValue: any): any {
    if (!value) {
      return defaultValue;
    }
    return value;
  }

}
